# Site pour le mémorandum Covid-19 pour du libre et de l’open en conscience : enseignements et impulsions futures

## Rappel initiatives

En cette période de crise et de confinement, de nombreuses initiatives portées par les mouvements du libre, de l’« open (science/data, hardware) » et des communs ont été (re)découvertes pour répondre aux besoins numériques repensés au regard des enjeux sociaux actuels. Complémentaires ou alternatifs aux dispositifs publics, ces courants communautaires de solidarité témoignent ainsi de leur pertinence à répondre à des besoins d’organisation rapide et de manière pragmatique.

Par ce mémorandum commun, nous souhaitons :

- rappeler en quoi ces idées sont particulièrement nécessaires dans la situation actuelle, mais aussi pourquoi elles impliquent une réflexion sur nos organisations sociales qu'il s'agit de construire avec différentes parties prenantes (acteurs proches, grand public, acteurs du numérique et gouvernement/politique).

- participer à la construction d'une vision commune sur la place du Libre / Open et des communs en ce temps de crise. Rappeler nos valeurs et points communs sera utile pour donner du sens aux actions collectives futures, face aux utilisateurs et utilisatrices de ces solutions et aux décideurs / politiques qui en cette période revoient nécessairement leurs idées préconçues.


Par « nous », nous entendons les communautés œuvrant aujourd'hui en se fondant sur les principes communs de la libre circulation de l'information, de gouvernance ouverte et de modèles sociaux durables.

## Organisation du projet

Ce projet soutient le site de présentation du mémorandum, il contient dans le dossier [content](./content)
  - le texte du [mémorandum](./content/page/memo.md)
  - les [impulsions](./content/page/impulsions.md)
  - les [soutiens](./content/page/soutien.md) qui ont publié sur leur site
  - un index résumant l'[objectif de l'initiative](./content/post/_index.md)

## Participation

Si vous souhaitez contribuer, soumettre des modifications au mémorandum, soutenir l'intiative. Vous pouvez directement le faire via une demande de merge (*merge request*) sur le [répertoire correspondant](https://framagit.org/covid19-open/memo) ou en contactant directement les soutiens à ce projet.

##  Mention légale du site  

Le contenu du site est mis à disposition selon les termes de la [Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).
