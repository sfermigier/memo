## Soutien

Le texte qui suit a été nourri de la contribution d'une trentaine de personnes œuvrant aujourd'hui en se fondant sur les principes communs de la libre circulation de l'information, de gouvernance ouverte et de modèles sociaux durables.

Plusieurs organisations ont traduit leur soutien par la publication du mémorandum et/ou des enseignements et mesures sur leur site.

Liste (provisoire) des organisations :
- Inno³
- HackYourResearch
- Wikimedia
- La MYNE
- Framasoft
- ADULLACT
- ADOA
- La Fabrique des Mobilités
- Covid-initiatives.org
- LibreAccès
